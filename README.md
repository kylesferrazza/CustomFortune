# CustomFortune
This plugin gives players a chance to have a console command run when they break certain blocks.

## Commands
This plugin does not contain any commands.

## Permissions
Permissions are layed out as follows:
<pre>
customfortune.1
customfortune.2
customfortune.3
</pre>
and so on. The number following 'customfortune.' is the % chance that the player will get lucky and the command(s) will be run.

## Config
Here is the default config:
<pre>
fortune-blocks:
  - 'diamond_ore'
  - 'iron_ore'
  - 'coal_ore'
  - 'emerald_ore'
  - 'lapis_ore'
message: '&2&oThe odds are in your favor!'
commands-to-run:
  - 'say &c&lPlayer ''%p'' broke %b and got lucky! They get a prize!'
  - 'give %p diamond 64'
</pre>
The fortune blocks section can be any length, with any material name in the game.
Material names can be found here: <a href="http://jd.bukkit.org/rb/apidocs/org/bukkit/Material.html">http://jd.bukkit.org/rb/apidocs/org/bukkit/Material.html</a>.

The string in 'message:' will be sent to the player if they break a fortune block and they end up succeeding to perform the command.

For the commands, use %p to substitute the player's name, and %b to substitute the name of the block they broke.
For example, the default first command will tell all players on the server that someone got lucky and what block they broke.

Chat colors are supported in both commands (for sending messages and broadcasting) and the message that will be sent to the player that broke the block.