package io.github.tubakyle.customfortune;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Kyle Sferrazza on 8/7/2014.
 * This file is a part of: CustomFortune.
 * All rights reserved.
 */

public class CustomFortune extends JavaPlugin {
    @Override
    public void onEnable() {
        saveDefaultConfig();
        FortuneConfiguration fc = new FortuneConfiguration(this);
        BreakListener bl = new BreakListener(this, fc);
        getServer().getPluginManager().registerEvents(bl, this);
    }
}
