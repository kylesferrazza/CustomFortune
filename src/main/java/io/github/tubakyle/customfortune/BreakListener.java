package io.github.tubakyle.customfortune;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

/**
 * Created by Kyle Sferrazza on 8/7/2014.
 * This file is a part of: CustomFortune.
 * All rights reserved.
 */
public class BreakListener implements Listener {

    private final CustomFortune plugin;
    private final FortuneConfiguration config;

    public BreakListener (CustomFortune plugin, FortuneConfiguration config) {
        this.plugin = plugin;
        this.config = config;
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {

        Player player = event.getPlayer();
        Material breakType = event.getBlock().getType();
        Material fortuneMaterial = null;
        for (String fortuneMaterialName : config.getMaterialsList()) {
            if (breakType.name().equalsIgnoreCase(fortuneMaterialName)) {
                fortuneMaterial = breakType;
            }
        }
        if (fortuneMaterial == null) {
            return;
        }
        int cmdChance = 0;
        for (int x = 1; x<101; x++) {
            if (player.hasPermission("customfortune." + Integer.toString(x))) {
                cmdChance = x;
                break;
            }
        }
        if (cmdChance == 0) {
            return;
        }
        if (isLucky(cmdChance)) {
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', config.getMessage()));
            String blockName = fortuneMaterial.name();
            blockName = blockName.substring(0, 1).toUpperCase() + "" + blockName.substring(1).toLowerCase();
            blockName = blockName.replace('_', ' ');
            for (String cmd : config.getCommandsToRun()) {
                cmd = cmd.replace("%p", player.getName());
                cmd = cmd.replace("%b", blockName);
                cmd = ChatColor.translateAlternateColorCodes('&', cmd);
                Bukkit.dispatchCommand(plugin.getServer().getConsoleSender(), cmd);
            }
        }
    }

    public boolean isLucky(int percentage) {
        double randD = Math.random() * 101;
        int rand = (int) randD;
        return rand < percentage;
    }
}
