package io.github.tubakyle.customfortune;

import java.util.List;

/**
 * Created by Kyle Sferrazza on 8/7/2014.
 * This file is a part of: CustomFortune.
 * All rights reserved.
 */
public class FortuneConfiguration {

    private final CustomFortune plugin;

    public FortuneConfiguration (CustomFortune instance) {
        this.plugin = instance;
    }

    public List<String> getMaterialsList() {
        return plugin.getConfig().getStringList("fortune-blocks");
    }

    public List<String> getCommandsToRun() {
        return plugin.getConfig().getStringList("commands-to-run");
    }

    public String getMessage() {
        return plugin.getConfig().getString("message");
    }

}
